# Changelog

## develop

## 1.1.0
* ~deprecated `HooksApi#getHooks`, `#getHook`, `#addHook`, `#editHook` and `#deleteHook`.
  - Renamed to `#getAll`, `#get`, `#create`, `#edit` and `#delete`.
* ~deprecated `RepositoryApi#getFile(path: string, ifExists: true)`.
  - Use `#tryGetFile`.
* Added `#tryGet` methods to `BranchesApi`, `CommitsApi`, `GroupApi`, `HooksApi`, `MergeRequestsApi`, `ProjectApi`, `TagsApi` and `UsersApi` (and `RepositoryApi#tryGetFile` and `MergeRequestsApi#tryGetVersion`)
  - Equivalent of existing `#get` (or similar) methods, except they return `undefined` if they recieve a 404 response.

## 1.0.1
* Fix `RepositoryApi#getFileContent`.
  * Handle `text/plain` responses.
  * Fix parsing of `Content-Length` header.

## 1.0.0
* Fix name of `ListProjectParams#order_by` parameter (was `#orderBy`)
* Add pagination parameters to operations which return lists.
* Added `JiraServiceApi`.

## 0.3.0
* [breaking] Use GitLab v4 API.
  - Removed `ProjectsApi#search`, `#getVisible`, `#getOwned`, `#getStarred`, `#getAllAdmin`.
  - `ListMergeRequestsParams#iid` to `#iids`.
  - `merge_when_build_succeeds` parameters to `merge_when_pipeline_succeeds`.
  - `MergeRequest#merge_when_build_succeeds` to `#merge_when_pipeline_succeeds`.
  - Remove `#public` from `CreateProjectParams` and `EditProjectParams`.
  - Replaced `#visiblity_level` with `#visibility`.
  - `build` -> `job`.
  - `MergeRequestApi` requires MR's IID, not ID.
  - Add new params to `CreateProjectParams`/`EditProjectParams`
  - Replaced `Issues#assignee` with `#assignees`.
* [breaking] GitLab base URL must not include API version.
* Add `ProjectApi#fork`.

## 0.2.2
* Fix `CommitsApi#create` operation (#2).

## 0.2.1
* Fix `CommitsApi#create` operation (#2).

## 0.2.0
* Add Commits API (#1, !2, !3).
* Add partial support for Services API (!4).
* [internal] Use Yarn for builds (!1).

## 0.1.0
* Initial release
