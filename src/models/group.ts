import { Project } from './project';
import { Visibility } from './visibility';

export interface Group {
  id: number;
  name: string;
  path: string;
  description?: string;
  visibility: Visibility;
  avatar_url?: string;
  web_url: string;
  request_access_enabled: boolean;
  full_name: string;
  full_path: string;
  parent_id?: number;
  projects: Project[];
}
