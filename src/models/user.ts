export interface UserSummary {
  id: number;
  username: string;
  name: string;
  state: 'active';
  created_at?: string;
  avatar_url?: string;
  web_url?: string;
  is_admin?: boolean;
  bio?: string;
  location?: string;
  skype?: string;
  linkedin?: string;
  twitter?: string;

}
export interface User extends UserSummary {
  website_url: string;
  organization: string;
  last_signed_in_at: string;
  confirmed_at: string;
  theme_id: number;
  color_scheme_id: number;
  projects_limit: number;
  current_sign_in_at: string;
  identities: UserIdentity[];
  can_create_group: boolean;
  can_create_project: boolean;
  two_factor_enabled: boolean;
  external: boolean;
}

export interface UserIdentity {
  provider: string;
  extern_uid: string;
}

export interface Contributor {
  name: string;
  email: string;
  commits: number;
  additions: number;
  deletions: number;
}
