export type Visibility = 'private' | 'internal' | 'public';
