export interface Label {
  id: number;
  color: string;
  name: string;
  description: string;
  open_issues_count: number;
  closed_issues_count: number;
  open_merge_requests_count: number;
  subscribed: boolean;
  priority?: number;
}
