import { Project } from './project';
import { UserSummary } from './user';

export interface Todo<T> {
  id: number;
  project: Project;
  author: UserSummary;
  action_name: string;
  target_type: string;
  target: T;
  target_url: string;
  body: string;
  state: string;
  create_at: string;
}
