import { Commit } from './commit';

export interface Branch {
  name: string;
  commit: Commit;
  protected: boolean;
  developers_can_push: boolean;
  developers_can_merge: boolean;
}
