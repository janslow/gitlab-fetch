import { JobState } from './job';
import { UserSummary } from './user';

// TODO: Can this be merged with Job?
export interface CommitStatus {
  author: UserSummary;
  name: string;
  sha: string;
  status: JobState;
  coverage?: number;
  description?: string;
  id: string;
  target_url?: string;
  ref?: string;
  started_at?: string;
  created_at?: string;
  allow_failure: boolean;
  finished_at?: string;
}
