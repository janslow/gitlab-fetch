import { Commit } from './commit';

export interface Tag {
  name: string;
  message: string;
  commit: Commit;
  release?: Release;
}

export interface Release {
  tag_name: string;
  description: string;
}
