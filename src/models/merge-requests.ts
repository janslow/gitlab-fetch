import { UserSummary } from './user';
import { Milestone } from './milestone';
import { Commit, Diff } from './commit';

export interface MergeRequestSummary {
  id: number;
  iid: number;
  target_branch: string;
  source_branch: string;
  project_id: number;
  title: string;
  state: 'merged' | 'opened' | 'closed';
  upvotes: number;
  downvotes: number;
  author: UserSummary;
  assignee?: UserSummary;
  source_project_id: number;
  target_project_id: number;
  labels: string[];
  description: string;
  work_in_progress: boolean;
  milestone: Milestone;
  merge_when_pipeline_succeeds: boolean;
  merge_status: string;
  sha: string;
  merge_commit_sha: string;
  user_notes_count: number;
  should_remove_source_branch: boolean;
  force_remove_source_branch: boolean;
  web_url: string;
}

export interface MergeRequest extends MergeRequestSummary {
  subscribed: boolean;
}

export interface MergeRequestVersionSummary {
  id: number;
  head_commit_sha: string;
  base_commit_sha: string;
  start_commit_sha: string;
  created_at: string;
  merge_request_id: number;
  state: string;
  real_size: string;
}

export interface MergeRequestVersion extends MergeRequestVersionSummary {
  commits: Commit[];
  diffs: Diff[];
}
