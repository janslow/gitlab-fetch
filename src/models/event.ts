import { UserSummary } from './user';

export interface Event {
  'title'?: string;
  project_id: number;
  action_name: string;
  target_id: number;
  target_type: string;
  author_id: number;
  data?: any;
  target_title: string;
  author: UserSummary;
  author_username: string;
}
