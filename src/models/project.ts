import { UserSummary } from './user';
import { Visibility } from './visibility';

export interface Project {
  id: number;
  description?: string;
  default_branch?: string;
  'public': boolean;
  visibility: Visibility;
  ssh_url_to_repo: string;
  http_url_to_repo: string;
  web_url: string;
  tag_list: string[];
  owner?: UserSummary;
  namespace: {
    created_at: string;
    description: string;
    id: number;
    name: string;
    owner_id: number;
    path: string;
    updated_at: string;
  };
  name: string;
  name_with_namespace: string;
  path: string;
  path_with_namespace: string;
  issues_enabled: boolean;
  open_issues_count: number;
  merge_requests_enabled: boolean;
  jobs_enabled: boolean;
  wiki_enabled: boolean;
  snippets_enabled: boolean;
  container_registry_enabled: boolean;
  created_at: string;
  last_activity_at: string;
  creator_id: number;
}
