export interface Commit {
  id: string;
  short_id?: string;
  parent_ids?: string;
  title: string;
  author_name: string;
  author_email: string;
  authored_date?: string;
  committer_name?: string;
  committer_email?: string;
  committed_date?: string;
  created_at?: string;
  message: string;
}

export interface Diff {
  old_path: string;
  new_path: string;
  a_mode: string;
  b_mode: string;
  diff: string;
  new_file: boolean;
  renamed_file: boolean;
  deleted_file: boolean;
}
