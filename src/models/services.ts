export interface Service {
  id?: number;
  title: string;
  created_at?: string;
  updated_at?: string;
  active: boolean;
  push_events: boolean;
  issues_events: boolean;
  merge_requests_events: boolean;
  tag_push_events: boolean;
  note_events: boolean;
  job_events: boolean;
  pipeline_events: boolean;
}
export interface InactiveService {
  active: false;
}
export interface ActiveService {
  id: number;
  created_at: string;
  updated_at: string;
  active: true;
  properties: {};
}
export interface CustomIssueTracker extends ActiveService {
  properties: {
    title: string;
    description: string;
    project_url: string;
    issues_url: string;
    new_issues_url: string;
  };
}
export interface JiraService extends ActiveService {
  properties: {
    url: string;
    api_url?: string;
    username?: string;
    jira_issue_transition_id?: number;
  };
}
