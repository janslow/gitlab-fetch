import { UserSummary } from './user';

export interface Comment {
  note: string;
  author: UserSummary;
  line_type?: 'new' | 'old';
  // TODO: parse as date
  created_at?: string;
  line?: number;
  path?: string;
}
