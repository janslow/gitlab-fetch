export interface PrivateKeyAuthentication {
  privateToken: string;
}

export interface OAuth2Authentication {
  accessToken: string;
}

export type Authentication = PrivateKeyAuthentication | OAuth2Authentication;

export interface Config {
  baseUrl: string;
  auth: Authentication;
}

/**
 * @internal
 */
export function isPrivateKeyAuthentication(maybe: Authentication): maybe is PrivateKeyAuthentication {
  const assume = maybe as PrivateKeyAuthentication;
  return assume.privateToken !== undefined;
}
