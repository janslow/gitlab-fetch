import fetch, { Response, Headers } from 'node-fetch';
import { stringify as stringifyQuery } from 'query-string';

import { Config, isPrivateKeyAuthentication } from './config';
import { network as debug } from './debug';

/**
 * @internal
 */
export type Method = 'GET' | 'POST' | 'PUT' | 'DELETE';
type CleanParam = string | number | boolean;

/**
 * @internal
 */
export interface FetchOptions {
  /**
   * Override the base API path for the request.
   */
  baseApiPath?: string;
  /**
   * Request body.
   */
  body?: any;
  /**
   * If true, return undefined if the response status is 404.
   */
  undefined404?: boolean;
}

export abstract class AbstractApi {
/**
 * @internal
 */
  private static cleanParams(params: { [key: string]: any }): { [key: string]: CleanParam|CleanParam[] } {
    const clean: { [key: string]: CleanParam|CleanParam[] } = {};
    for (const key of Object.keys(params)) {
      const value = params[key];
      if (value !== undefined) {
        clean[key] = value instanceof Array ? value.map(AbstractApi.cleanParam) : AbstractApi.cleanParam(value);
      }
    }
    return clean;
  }

  /**
   * @internal
   */
  private static cleanParam(value: any): CleanParam {
    if (typeof value === 'string' || typeof value === 'number' || typeof value === 'boolean') {
      return value;
    }
    else if (value instanceof Date) {
      return value.toISOString();
    }
    throw new Error(`Unable to serialize parameter value ${value}`);
  }

  /**
   * @internal
   */
  private static async parseBody(response: Response): Promise<any> {
    const contentType = response.headers.get('Content-Type');
    if (contentType === 'application/json') {
      return await response.json();
    }
    const contentSize = parseInt(response.headers.get('Content-Length'), 10) || 0;
    if (contentSize === 0) {
      return undefined;
    }
    else if (contentType === 'application/octet' || contentType === 'text/plain') {
      return await response.buffer();
    }
    debug(response);
    throw new Error(`Unexpected content type "${contentType}"`);
  }

  /**
   * @param config - Auth/deployment configuration.
   * @param baseApiPath - Base path of the API, to prepend to all requests.
   * @internal
   */
  constructor(public config: Config, protected readonly baseApiPath: string) {
  }

  /**
   * Make a HTTP request and check that the response status is 2xx.
   * @param method - HTTP method.
   * @param path - Path to append to baseApiPath, or undefined to just use the baseApiPath.
   * @param params - Query params to add to URL. Stringified using the query-string package.
   * @param options - Extra request options
   * @return Promise of parsed response body (parsed JSON, Buffer or undefined).
   * @throws Error if response status is not 2xx.
   * @internal
   */
  protected async fetch(method: Method, path: string | undefined, params = {}, options: FetchOptions = {}): Promise<any> {
    const [response, body] = await this.fetchUnchecked(method, path, params, options);
    if (response.status === 404 && options.undefined404) {
      debug(' => (Ignored)');
      return undefined;
    }
    if (response.status < 200 || response.status >= 300) {
      debug(' => %O', body);
      throw new Error(`Unexpected HTTP status (${response.status}) from ${response.url}`);
    }
    return body;
  }

  /**
   * Make a HTTP request.
   * @param method - HTTP method.
   * @param path - Path to append to baseApiPath, or undefined to just use the baseApiPath.
   * @param params - Query params to add to URL. Stringified using the query-string package.
   * @param options - Extra request options
   * @return Promise of response and parsed body (parsed JSON, Buffer or undefined).
   * @internal
   */
  protected async fetchUnchecked(
    method: Method, path: string | undefined, params = {}, options: FetchOptions = {},
  ): Promise<[Response, any]> {
    const response = await this.rawFetch(method, path || '', params, undefined, options);
    const body = await AbstractApi.parseBody(response);
    return [response, body];
  }

  /**
   * @internal
   */
  private enhanceHeaders(headers: Headers): void {
    const { auth } = this.config;
    if (isPrivateKeyAuthentication(auth)) {
      headers.append('PRIVATE-TOKEN', auth.privateToken);
    }
    else {
      headers.append('Authentication', `Bearer ${auth.accessToken}`);
    }
  }

  /**
   * @internal
   */
  private async rawFetch(method: Method, path: string, params: {}, headers = new Headers(), options: FetchOptions = {}): Promise<Response> {
    const { baseApiPath = this.baseApiPath, body } = options;
    this.enhanceHeaders(headers);
    let url = `${this.config.baseUrl}/v4${baseApiPath}${path}`;
    const query = stringifyQuery(AbstractApi.cleanParams(params));
    if (query) {
      url = `${url}?${query}`;
    }
    debug('%s %s', method, url);
    let requestBody = undefined;
    if (body !== undefined) {
      requestBody = JSON.stringify(body);
      headers.set('Content-Type', 'application/json');
    }
    const response = await fetch(url, {
      method,
      headers,
      body: requestBody,
    });
    debug('=> %d', response.status);
    return response;
  }
}
