import createDebug = require('debug');

/**
 * @internal
 */
export const network = createDebug('gitlab-fetch:network');

/**
 * @internal
 */
export const loadConfig = createDebug('gitlab-fetch:load-config');
