import rc = require('rc');

import { Config } from './config';
import { loadConfig as debug } from './debug';

export interface RawConfig {
  gitlab?: {
    baseUrl?: string;
    privateToken?: string;
  };
}

const APP_NAME = 'gitlab-fetch';

interface RCMetadata {
  configs?: string[];
  config?: string;
}

class ConfigError extends Error {
  constructor(path: string | undefined, detail: string) {
    let message = 'Invalid config';
    if (path !== undefined) {
      message = `${message} (loaded from ${path})`;
    }
    message = `${message}: ${detail}`;
    super(detail);
    this.name = 'ConfigError';
    Error.captureStackTrace(this, this.constructor);
  }
}

let cache: Config | undefined;

export default function load(defaultOptions: RawConfig = {}): Config | never {
  if (!cache) {
    const { gitlab, config: path } = rc(APP_NAME, defaultOptions) as RCMetadata & RawConfig;
    debug(`Loaded config from "${path}"`, gitlab);
    if (typeof gitlab !== 'object') {
      throw new ConfigError(path, '"gitlab" property must be an object');
    }
    const { baseUrl, privateToken } = gitlab;
    if (typeof baseUrl !== 'string') {
      throw new ConfigError(path, '"gitlab.baseUrl" property must be a string');
    }
    else if (baseUrl.endsWith('/')) {
      throw new ConfigError(path, '"gitlab.baseUrl" property must not have a trailing slash');
    }
    else if (/\/v\d+$/.test(baseUrl)) {
      throw new ConfigError(path, '"gitlab.baseUrl" property must not end with API version.');
    }
    else if (typeof privateToken !== 'string') {
      throw new ConfigError(path, '"gitlab.privateToken" property must be a string');
    }
    cache = {
      auth: {
        privateToken,
      },
      baseUrl,
    };
  }
  return cache;
}
