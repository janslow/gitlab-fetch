
/**
 * @internal
 */
export function encode(raw: string): string {
  return encodeURIComponent(raw).replace(/[!'()*.]/g, c => `%${c.charCodeAt(0).toString(16)}`);
}
