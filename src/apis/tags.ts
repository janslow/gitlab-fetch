import { AbstractApi, Config } from '../common';
import { encode } from '../common/utils';
import { Tag, Release } from '../models';
import { ProjectApi } from './project';
import { PaginationParams } from './pagination';

export interface CreateTagParams {
  message?: string;
  release_description?: string;
}

export class TagsApi extends AbstractApi {
  constructor(config: Config, id: number);
  constructor(config: Config, namespace: string, projectName: string);
  constructor(config: Config, idOrNamespace: number|string, projectName?: string) {
    super(config, `/projects/${ProjectApi.toProjectId(idOrNamespace, projectName)}/repository/tags`);
  }

  getAll(params: PaginationParams = {}): Promise<Tag[]> {
    return this.fetch('GET', undefined, params);
  }

  get(name: string): Promise<Tag> {
    return this.fetch('GET', `/${encode(name)}`);
  }
  tryGet(name: string): Promise<Tag | undefined> {
    return this.fetch('GET', `/${encode(name)}`, undefined, { undefined404: true });
  }

  create(name: string, ref: string, params: CreateTagParams = {}): Promise<Tag> {
    return this.fetch('POST', undefined, Object.assign({ tag_name: name, ref }, params));
  }
  delete(name: string): Promise<void> {
    return this.fetch('DELETE', `/${encode(name)}`);
  }

  createRelease(name: string, description: string): Promise<Release> {
    return this.fetch('POST', `/${encode(name)}/release`, { description });
  }

  editRelease(name: string, description: string): Promise<Release> {
    return this.fetch('PUT', `/${encode(name)}/release`, { description });
  }
}
