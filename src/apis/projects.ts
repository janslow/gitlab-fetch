import { AbstractApi, Config } from '../common';

import { Project, Visibility } from '../models';
import { PaginationParams } from './pagination';

export interface ListProjectsParams extends PaginationParams {
    archived?: boolean;
    visibility?: Visibility;
    order_by?: 'id' | 'name' | 'path' | 'created_at' | 'updated_at' | 'last_activity_at';
    sort?: 'asc' | 'desc';
    search?: string;
    simple?: boolean;
    owned?: boolean;
    membership?: boolean;
    starred?: boolean;
    statistics?: boolean;
    with_issues_enabled?: boolean;
    with_merge_requests_enabled?: boolean;
}

export interface CreateProjectParams {
    path?: string;
    namespace_id?: number;
    description?: string;
    issues_enabled?: boolean;
    merge_requests_enabled?: boolean;
    jobs_enabled?: boolean;
    wiki_enabled?: boolean;
    snippets_enabled?: boolean;
    container_registry_enabled?: boolean;
    shared_runners_enabled?: boolean;
    visibility?: Visibility;
    import_url?: string;
    public_jobs?: boolean;
    only_allow_merge_if_pipeline_succeeds?: boolean;
    only_allow_merge_if_all_discussions_are_resolved?: boolean;
    lfs_enabled?: boolean;
    request_access_enabled?: boolean;
    tag_list?: string[];
    // avatar?: TODO
    printing_merge_request_link_enabled?: boolean;
    ci_config_path?: string;
}

export class ProjectsApi extends AbstractApi {
  constructor(config: Config) {
    super(config, '/projects');
  }

  getAll(params: ListProjectsParams = {}): Promise<Project[]> {
    return this.fetch('GET', undefined, params);
  }

  create(name: string, params: CreateProjectParams = {}): Promise<Project> {
    return this.fetch('POST', undefined, Object.assign({ name }, params));
  }

  createAdmin(userId: number, name: string, params: CreateProjectParams = {}): Promise<Project> {
    return this.fetch('POST', `/user/${userId}`, Object.assign({ name }, params));
  }
}
