import { AbstractApi, Config } from '../common';
import { RepositoryFile, RepositoryTreeEntry, CompareRepository, Contributor } from '../models';
import { ProjectApi } from './project';
import { PaginationParams } from './pagination';

import { encode } from '../common/utils';

export interface GetTreeParams extends PaginationParams {
  path?: string;
  recursive?: boolean;
}

export interface DeleteFileParams {
  author_email?: string;
  author_name?: string;
}
export interface CreateFileParams extends DeleteFileParams {
  encoding?: 'base64' | undefined;
}
export interface EditFileParams extends CreateFileParams {
}

export class RepositoryApi extends AbstractApi {
  public readonly ref: string;

  constructor(config: Config, id: number, ref: string);
  constructor(config: Config, namespace: string, projectName: string, ref: string);
  constructor(config: Config, idOrNamespace: number|string, projectNameOrRef: string, maybeRef?: string) {
    let projectName: string | undefined;
    let ref: string;
    if (maybeRef === undefined) {
      ref = projectNameOrRef;
      projectName = undefined;
    }
    else {
      ref = maybeRef;
      projectName = projectNameOrRef;
    }
    super(config, `/projects/${ProjectApi.toProjectId(idOrNamespace, projectName)}/repository`);
    this.ref = ref;
  }

  getTree(params: GetTreeParams = {}): Promise<RepositoryTreeEntry[]> {
    return this.fetch('GET', '/tree', Object.assign({ ref: this.ref }, params));
  }

  getFile(path: string): Promise<RepositoryFile>;
  /** @deprecated Use #tryGetFile instead. */
  getFile(path: string, ifExists: true): Promise<RepositoryFile | undefined>;
  async getFile(path: string, ifExists = false): Promise<RepositoryFile | undefined> {
    return this.fetch('GET', `/files/${encode(path)}`, { ref: this.ref }, { undefined404: ifExists });
  }

  tryGetFile(path: string): Promise<RepositoryFile | undefined> {
    return this.getFile(path, true);
  }

  getFileContent(path: string): Promise<Buffer> {
    return this.fetch('GET', `/files/${encode(path)}/raw`, { ref: this.ref });
  }

  getBlobContent(sha: string): Promise<Buffer> {
    return this.fetch('GET', `/blobs/${encode(sha)}/raw`);
  }

  createFile(path: string, content: string, commitMessage: string, params: CreateFileParams = {}): Promise<Object> {
    const fullParams = Object.assign({ file_path: path, branch: this.ref, content, commit_message: commitMessage}, params);
    return this.fetch('POST', '/files', fullParams);
  }

  editFile(path: string, content: string, commitMessage: string, params: EditFileParams = {}): Promise<Object> {
    const fullParams = Object.assign(
      { file_path: path, branch: this.ref, content, commit_message: commitMessage },
      params,
    );
    return this.fetch('PUT', '/files', fullParams);
  }

  deleteFile(path: string, commitMessage: string, params: DeleteFileParams = {}): Promise<Object> {
    const fullParams = Object.assign({ file_path: path, branch: this.ref, commit_message: commitMessage}, params);
    return this.fetch('DELETE', '/files', fullParams);
  }

  getArchive(): Promise<Buffer> {
    return this.fetch('GET', '/archive', { sha: this.ref });
  }

  compare(from: string, to: string): Promise<CompareRepository> {
    return this.fetch('GET', '/compare', { from, to });
  }

  getContributors(params: PaginationParams = {}): Promise<Contributor[]> {
    return this.fetch('GET', '/contributors', params);
  }
}
