import { AbstractApi, Config } from '../common';
import { Hook } from '../models';
import { ProjectApi } from './project';
import { PaginationParams } from './pagination';

export interface AddHookParams {
  push_events?: boolean;
  issues_events?: boolean;
  merge_requests_events?: boolean;
  tag_push_events?: boolean;
  note_events?: boolean;
  job_events?: boolean;
  pipeline_events?: boolean;
  wiki_events?: boolean;
  enable_ssl_verification?: boolean;
  token?: string;
}

export interface EditHookParams extends AddHookParams {
}

export class HooksApi extends AbstractApi {
  /** @deprecated Use #getAll instead */
  getHooks = this.getAll;
  /** @deprecated Use #get instead */
  getHook = this.get;
  /** @deprecated Use #create instead */
  addHook = this.create;
  /** @deprecated Use #edit instead */
  editHook = this.edit;
  /** @deprecated Use #delete instead */
  deleteHook = this.delete;

  constructor(config: Config, id: number);
  constructor(config: Config, namespace: string, projectName: string);
  constructor(config: Config, idOrNamespace: number|string, projectName?: string) {
    super(config, `/projects/${ProjectApi.toProjectId(idOrNamespace, projectName)}/hooks`);
  }

  getAll(params: PaginationParams = {}): Promise<Hook[]> {
    return this.fetch('GET', undefined, params);
  }

  get(hookId: number): Promise<Hook> {
    return this.fetch('GET', `/${hookId}`);
  }
  tryGet(hookId: number): Promise<Hook | undefined> {
    return this.fetch('GET', `/${hookId}`, undefined, { undefined404: true });
  }

  create(url: string, params: AddHookParams = {}): Promise<Hook> {
    return this.fetch('POST', ``, Object.assign({ url }, params));
  }
  edit(hookId: number, url: string, params: EditHookParams = {}): Promise<Hook> {
    return this.fetch('POST', `/${hookId}`, Object.assign({ url }, params));
  }
  delete(hookId: number): Promise<void> {
    return this.fetch('DELETE', `/${hookId}`);
  }
}
