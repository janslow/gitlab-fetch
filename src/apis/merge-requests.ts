import { AbstractApi, Config } from '../common';
import { MergeRequestSummary, MergeRequest, Commit, Issue, Todo, MergeRequestVersionSummary, MergeRequestVersion } from '../models';
import { ProjectApi } from './project';
import { PaginationParams } from './pagination';

export interface ListMergeRequestsParams extends PaginationParams {
  iids?: number[];
  state?: 'all' | 'merged' | 'opened' | 'closed';
  order_by?: 'created_at' | 'updated_at';
  sort?: 'asc' | 'desc';
}

export interface CreateMergeRequestsParams {
  assignee_id?: number;
  description?: string;
  target_project_id?: string;
  labels?: string[];
  milestone_id?: number;
  remove_source_branch?: true | undefined;
}

export interface EditMergeRequestParams extends CreateMergeRequestsParams {
  source_branch?: string;
  target_branch?: string;
  title?: string;
}

export interface AcceptMergeRequestParams {
  merge_commit_message?: string;
  should_remove_source_branch?: boolean;
  merge_when_pipeline_succeeds?: boolean;
  sha?: string;
}

export class MergeRequestsApi extends AbstractApi {
  constructor(config: Config, id: number);
  constructor(config: Config, namespace: string, projectName: string);
  constructor(config: Config, idOrNamespace: number|string, projectName?: string) {
    super(config, `/projects/${ProjectApi.toProjectId(idOrNamespace, projectName)}/merge_requests`);
  }

  getAll(params: ListMergeRequestsParams = {}): Promise<MergeRequestSummary[]> {
    return this.fetch('GET', undefined, params);
  }
  get(iid: number): Promise<MergeRequest> {
    return this.fetch('GET', `/${iid}`);
  }
  tryGet(iid: number): Promise<MergeRequest | undefined> {
    return this.fetch('GET', `/${iid}`, undefined, { undefined404: true });
  }
  getCommits(iid: number, params: PaginationParams = {}): Promise<Commit[]> {
    return this.fetch('GET', `/${iid}/commits`, params);
  }
  getChanges(iid: number): Promise<MergeRequest> {
    return this.fetch('GET', `/${iid}/changes`);
  }
  getIssues(iid: number, params: PaginationParams = {}): Promise<Issue[]> {
    return this.fetch('GET', `/${iid}/closes_issues`, params);
  }
  getVersions(iid: number): Promise<MergeRequestVersionSummary> {
    return this.fetch('GET', `/${iid}/versions`);
  }
  getVersion(iid: number, versionId: number): Promise<MergeRequestVersion> {
    return this.fetch('GET', `/${iid}/versions/${versionId}`);
  }
  tryGetVersion(iid: number, versionId: number): Promise<MergeRequestVersion | undefined> {
    return this.fetch('GET', `/${iid}/versions/${versionId}`, undefined, { undefined404: true });
  }
  create(sourceBranch: string, targetBranch: string, title: string, params: CreateMergeRequestsParams = {}): Promise<MergeRequest> {
    return this.fetch('POST', undefined, Object.assign({ source_branch: sourceBranch, target_branch: targetBranch, title }, params));
  }
  edit(iid: number, params: EditMergeRequestParams = {}): Promise<MergeRequest> {
    return this.fetch('PUT', `/${iid}`, params);
  }
  delete(iid: number): Promise<void> {
    return this.fetch('DELETE', `/${iid}`);
  }
  accept(iid: number, params: AcceptMergeRequestParams = {}): Promise<MergeRequest> {
    return this.fetch('PUT', `/${iid}/merge`, params);
  }
  cancel(iid: number): Promise<MergeRequest> {
    return this.fetch('PUT', `/${iid}/cancel_merge_when_pipeline_succeeds`);
  }
  subscribe(iid: number): Promise<MergeRequest> {
    return this.fetch('POST', `/${iid}/subscription`);
  }
  unsubscribe(iid: number): Promise<MergeRequest> {
    return this.fetch('DELETE', `/${iid}/subscription`);
  }
  createTodo(iid: number): Promise<Todo<MergeRequest>> {
    return this.fetch('POST', `/${iid}/todo`);
  }
}
