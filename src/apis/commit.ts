import { AbstractApi, Config } from '../common';
import { encode } from '../common/utils';
import { Commit, Diff, Comment, CommitStatus } from '../models';
import { ProjectApi } from './project';
import { PaginationParams } from './pagination';

export interface ListCommitsParams extends PaginationParams {
  ref_name?: string;
  // TODO: Accept Date object
  since?: string;
  // TODO: Accept Date object
  until?: string;
}

export interface CreateCommitCommentParams {
  path?: string;
  line?: number;
  line_type?: 'new' | 'old';
}

export interface GetCommitStatusParams extends PaginationParams {
  ref?: string;
  stage?: string;
  name?: string;
  all?: boolean;
}

export interface SetCommitStatusParams {
  ref?: string;
  name?: string;
  target_url?: string;
  description?: string;
  coverage?: number;
}

export interface CreateCommitParams {
  author_name?: string;
  author_email?: string;
}

export type CommitAction = CommitCreateAction | CommitDeleteAction | CommitMoveAction | CommitUpdateAction;

export interface CommitCreateAction {
  action: 'create';
  file_path: string;
  content: string;
  encoding?: 'text' | 'base64';
}

export interface CommitDeleteAction {
  action: 'delete';
  file_path: string;
}

export interface CommitMoveAction {
  action: 'move';
  file_path: string;
  previous_path: string;
  content?: string;
  encoding?: 'text' | 'base64';
}

export interface CommitUpdateAction {
  action: 'update';
  file_path: string;
  content: string;
  encoding?: 'text' | 'base64';
}

/**
 * @see {@link https://docs.gitlab.com/ce/api/commits.html}
 */
export class CommitsApi extends AbstractApi {
  constructor(config: Config, id: number);
  constructor(config: Config, namespace: string, projectName: string);
  constructor(config: Config, idOrNamespace: number|string, projectName?: string) {
    super(config, `/projects/${ProjectApi.toProjectId(idOrNamespace, projectName)}/repository/commits`);
  }

  getAll(params: ListCommitsParams = {}): Promise<Commit[]> {
    return this.fetch('GET', undefined, params);
  }
  get(ref: string): Promise<Commit> {
    return this.fetch('GET', `/${encode(ref)}`);
  }
  tryGet(ref: string): Promise<Commit | undefined> {
    return this.fetch('GET', `/${encode(ref)}`, undefined, { undefined404: true });
  }
  create(branch: string, commitMessage: string, actions: CommitAction[], params: CreateCommitParams = {}): Promise<Commit> {
    return this.fetch('POST', undefined, {}, {
      body: {
        ...params,
        branch,
        commit_message: commitMessage,
        actions,
      },
    });
  }
  cherryPick(sha: string, branch: string): Promise<Commit> {
    return this.fetch('POST', `/${encode(sha)}/cherry_pick`, { branch });
  }
  getDiff(ref: string, params: PaginationParams = {}): Promise<Diff[]> {
    return this.fetch('GET', `/${encode(ref)}/diff`, params);
  }
  getComments(ref: string): Promise<Comment[]> {
    return this.fetch('GET', `/${encode(ref)}/comments`);
  }
  createComment(ref: string, note: string, params: CreateCommitCommentParams = {}): Promise<Comment> {
    return this.fetch('POST', `/${encode(ref)}/comments`, { ...params, note });
  }
  getStatus(sha: string, params: GetCommitStatusParams = {}): Promise<CommitStatus[]> {
    return this.fetch('GET', `/${encode(sha)}/statuses`, params);
  }
  setStatus(sha: string, params: SetCommitStatusParams = {}): Promise<CommitStatus> {
    return this.fetch('POST', `/${encode(sha)}/statuses`, params);
  }
}
