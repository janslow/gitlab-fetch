import { AbstractApi, Config } from '../common';
import { encode } from '../common/utils';
import { Label } from '../models';
import { ProjectApi } from './project';
import { PaginationParams } from './pagination';

export interface CreateLabelParams {
  description?: string;
  priority?: number | 'null';
}

export interface EditLabelParams extends CreateLabelParams {
  new_name?: string;
  color?: string;
}

export class LabelsApi extends AbstractApi {
  constructor(config: Config, id: number);
  constructor(config: Config, namespace: string, projectName: string);
  constructor(config: Config, idOrNamespace: number|string, projectName?: string) {
    super(config, `/projects/${ProjectApi.toProjectId(idOrNamespace, projectName)}/labels`);
  }

  getAll(params: PaginationParams = {}): Promise<Label[]> {
    return this.fetch('GET', undefined, params);
  }
  create(name: string, color: string, params: CreateLabelParams = {}): Promise<Label> {
    return this.fetch('POST', undefined, Object.assign({ name, color }, params));
  }
  edit(name: string, params: EditLabelParams = {}): Promise<Label> {
    return this.fetch('PUT', undefined, Object.assign({ name }, params));
  }
  delete(name: string): Promise<void> {
    return this.fetch('DELETE', undefined, { name });
  }
  subscribe(name: string): Promise<Label> {
    return this.fetch('POST', `/${encode(name)}/subscription`, { name });
  }
  unsubscribe(name: string): Promise<Label> {
    return this.fetch('DELETE', `/${encode(name)}/subscription`, { name });
  }
}
