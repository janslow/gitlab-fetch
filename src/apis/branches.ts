import { AbstractApi, Config } from '../common';
import { encode } from '../common/utils';
import { Branch } from '../models';
import { ProjectApi } from './project';
import { PaginationParams } from './pagination';

export interface ProtectBranchParams {
  developers_can_push?: boolean;
  developers_can_merge?: boolean;
}

export class BranchesApi extends AbstractApi {
  constructor(config: Config, id: number);
  constructor(config: Config, namespace: string, projectName: string);
  constructor(config: Config, idOrNamespace: number|string, projectName?: string) {
    super(config, `/projects/${ProjectApi.toProjectId(idOrNamespace, projectName)}/repository/branches`);
  }

  getAll(params: PaginationParams = {}): Promise<Branch[]> {
    return this.fetch('GET', undefined, params);
  }
  get(name: string): Promise<Branch> {
    return this.fetch('GET', `/${encode(name)}`);
  }
  tryGet(name: string): Promise<Branch | undefined> {
    return this.fetch('GET', `/${encode(name)}`, undefined, { undefined404: true });
  }
  protect(name: string, params: ProtectBranchParams = {}): Promise<Branch> {
    return this.fetch('PUT', `/${encode(name)}/protect`, params);
  }
  unprotect(name: string): Promise<Branch> {
    return this.fetch('PUT', `/${encode(name)}/unprotect`);
  }
  create(name: string, ref: string): Promise<Branch> {
    return this.fetch('POST', undefined, { branch: name, ref });
  }
  delete(name: string): Promise<void> {
    return this.fetch('DELETE', `/${encode(name)}`);
  }
  // deleteMergedBranches(): Promise<void> {
  //   return this.fetch('DELETE', TODO);
  // }
}
