import { AbstractApi, Config } from '../common';
import { encode } from '../common/utils';
import { Project, UserSummary, Visibility, Event, UploadedFile } from '../models';
import { PaginationParams } from './pagination';

export interface EditProjectParams {
  name?: string;
  path?: string;
  default_branch?: string;
  description?: string;
  issues_enabled?: boolean;
  merge_requests_enabled?: boolean;
  jobs_enabled?: boolean;
  wiki_enabled?: boolean;
  snippets_enabled?: boolean;
  container_registry_enabled?: boolean;
  shared_runners_enabled?: boolean;
  visibility?: Visibility;
  import_url?: string;
  public_jobs?: boolean;
  only_allow_merge_if_pipeline_succeeds?: boolean;
  only_allow_merge_if_all_discussions_are_resolved?: boolean;
  lfs_enabled?: boolean;
  request_access_enabled?: boolean;
  tag_list?: string[];
  // avatar?: TODO
  printing_merge_request_link_enabled?: boolean;
  ci_config_path?: string;
}

export interface ShareProjectParams {
  expires_at?: Date;
}

export class ProjectApi extends AbstractApi {
  /**
   * @internal
   */
  static toProjectId(idOrNamespace: number|string, projectName: undefined|string): string {
    if (typeof idOrNamespace === 'number') {
      return `${idOrNamespace}`;
    }
    else {
      return encode(`${idOrNamespace}/${projectName}`);
    }
  }

  constructor(config: Config, id: number);
  constructor(config: Config, namespace: string, projectName: string);
  constructor(config: Config, idOrNamespace: number|string, projectName?: string) {
    super(config, `/projects/${ProjectApi.toProjectId(idOrNamespace, projectName)}`);
  }

  get(): Promise<Project> {
    return this.fetch('GET', undefined);
  }
  tryGet(): Promise<Project | undefined> {
    return this.fetch('GET', undefined, undefined, { undefined404: true });
  }

  edit(params: EditProjectParams = {}): Promise<Project> {
    return this.fetch('PUT', undefined, params);
  }

  delete(): Promise<Project> {
    return this.fetch('DELETE', undefined);
  }

  star(): Promise<Project> {
    return this.fetch('POST', '/star');
  }

  unstar(): Promise<Project> {
    return this.fetch('POST', '/unstar');
  }

  archive(): Promise<Project> {
    return this.fetch('POST', '/archive');
  }

  unarchive(): Promise<Project> {
    return this.fetch('POST', '/unarchive');
  }

  getUsers(params: PaginationParams = {}): Promise<UserSummary[]> {
    return this.fetch('GET', '/users', params);
  }

  getEvents(params: PaginationParams = {}): Promise<Event[]> {
    return this.fetch('GET', '/events', params);
  }

  uploadFile(file: string): Promise<UploadedFile> {
    return this.fetch('POST', '/uploads', { file });
  }

  share(groupId: number, groupAccess: number, params: ShareProjectParams = {}): Promise<void> {
    return this.fetch('POST', '/share', Object.assign({ group_id: groupId, group_access: groupAccess }, params));
  }

  unshare(groupId: number): Promise<void> {
    return this.fetch('DELETE', `/share/${groupId}`);
  }
  fork(namespace: number|string): Promise<void> {
    return this.fetch('POST', `/fork`, { namespace });
  }
  createForkRelation(forkedFromId: number): Promise<void> {
    return this.fetch('POST', `/fork/${forkedFromId}`);
  }
  deleteForkRelation(): Promise<void> {
    return this.fetch('DELETE', `/fork`);
  }
}
