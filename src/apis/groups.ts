import { AbstractApi, Config } from '../common';
import { encode } from '../common/utils';
import { Group, Project, Visibility } from '../models';
import { PaginationParams } from './pagination';

export interface ListGroupsParams extends PaginationParams {
  skip_groups?: number[];
  all_available?: boolean;
  search?: string;
  order_by?: 'name' | 'path';
  sort?: 'asc' | 'desc';
  statistics?: boolean;
  owned?: boolean;
}

export interface ListGroupProjectsParams extends PaginationParams {
  archived?: boolean;
  visibility?: Visibility;
  order_by?: 'id' | 'name' | 'path' | 'created_at' | 'updated_at' | 'last_activity_at';
  sort?: 'asc' | 'desc';
  search?: string;
  simple?: boolean;
  owned?: boolean;
  starred?: boolean;
}

export interface CreateGroupParams {
  description?: string;
  visbility?: Visibility;
  lfs_enabled?: boolean;
  request_access_enabled?: boolean;
}

export class GroupsApi extends AbstractApi {
  constructor(config: Config) {
    super(config, `/groups`);
  }

  getAll(params: ListGroupsParams = {}): Promise<Group[]> {
    return this.fetch('GET', undefined, params);
  }

  create(name: string, path: string, params: CreateGroupParams = {}): Promise<Group> {
    return this.fetch('POST', undefined, { name, path, ...params });
  }
}

export class GroupApi extends AbstractApi {
  /**
   * @internal
   */
  static toGroupId(idOrPath: number|string): string {
    if (typeof idOrPath === 'number') {
      return `${idOrPath}`;
    }
    else {
      return encode(idOrPath);
    }
  }

  constructor(config: Config, id: number);
  constructor(config: Config, path: string);
  constructor(config: Config, idOrPath: number|string) {
    super(config, `/groups/${GroupApi.toGroupId(idOrPath)}`);
  }

  get(): Promise<Group> {
    return this.fetch('GET', undefined);
  }

  tryGet(): Promise<Group | undefined> {
    return this.fetch('GET', undefined, undefined, { undefined404: true });
  }

  getProjects(params: ListGroupProjectsParams = {}): Promise<Project[]> {
    return this.fetch('GET', '/projects', params);
  }

  delete(): Promise<void> {
    return this.fetch('DELETE', undefined);
  }
}
