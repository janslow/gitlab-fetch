import { AbstractApi, Config } from '../common';
import { CustomIssueTracker, ActiveService, InactiveService } from '../models';
import { ProjectApi } from './project';

export class AbstractServiceApi<TModel extends ActiveService, TEnable> extends AbstractApi {
  /**
   * @internal
   */
  constructor(service: string, config: Config, idOrNamespace: number|string, projectName?: string) {
    super(config, `/projects/${ProjectApi.toProjectId(idOrNamespace, projectName)}/services/${service}`);
  }

  disable(): Promise<void> {
    return this.fetch('DELETE', undefined);
  }

  get(): Promise<TModel | InactiveService> {
    return this.fetch('GET', undefined);
  }

  enable(params: TEnable): Promise<TModel> {
    return this.fetch('PUT', undefined, params);
  }
}

export interface EnableCustomIssueTrackerParams {
  new_issue_url: string;
  issues_url: string;
  project_url: string;
  description?: string;
  title?: string;
}

export class CustomIssueTrackerServiceApi extends AbstractServiceApi<CustomIssueTracker, EnableCustomIssueTrackerParams> {
  constructor(config: Config, id: number);
  constructor(config: Config, namespace: string, projectName: string);
  constructor(config: Config, idOrNamespace: number|string, projectName?: string) {
    super('custom-issue-tracker', config, idOrNamespace, projectName);
  }
}

export interface EnableJiraServiceApi {
  url: string;
  api_url?: string;
  username?: string;
  password?: string;
  jira_issue_transition_id?: number;
}

export class JiraServiceApi extends AbstractServiceApi<CustomIssueTracker, EnableJiraServiceApi> {
  constructor(config: Config, id: number);
  constructor(config: Config, namespace: string, projectName: string);
  constructor(config: Config, idOrNamespace: number|string, projectName?: string) {
    super('jira', config, idOrNamespace, projectName);
  }
}
