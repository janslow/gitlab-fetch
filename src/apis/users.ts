import { AbstractApi, Config } from '../common';
import { User, UserSummary } from '../models';
import { PaginationParams } from './pagination';

export interface GetUsersParams extends PaginationParams {
  active?: boolean;
  blocked?: boolean;
  search?: string;
  username?: string;
  external?: boolean;
}

export interface GetCurrentUserParams {
  sudo?: number;
}

export class UsersApi extends AbstractApi {
  constructor(config: Config) {
    super(config, '/users');
  }

  getAll(params: GetUsersParams = {}): Promise<(UserSummary[])|(User[])> {
    return this.fetch('GET', undefined, params);
  }

  get(id: number): Promise<UserSummary|User> {
    return this.fetch('GET', `/${id}`);
  }
  tryGet(id: number): Promise<UserSummary|User> {
    return this.fetch('GET', `/${id}`, undefined, { undefined404: true });
  }

  getCurrent(params: GetCurrentUserParams = {}): Promise<User> {
    return this.fetch('GET', undefined, params, { baseApiPath: '/user' });
  }

  delete(id: number): Promise<void> {
    return this.fetch('DELETE', `/${id}`);
  }

  getSSHKeys(params: PaginationParams = {}): Promise<Object[]> {
    return this.fetch('GET', '/keys', params, { baseApiPath: '/user' });
  }
}
