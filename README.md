A GitLab API client using the [whatwg-fetch API](https://github.com/whatwg/fetch) (written in TypeScript).

[![NPM](https://nodei.co/npm/gitlab-fetch.png)](https://nodei.co/npm/gitlab-fetch/)

## Currently implemented APIs
* [Projects](https://docs.gitlab.com/ce/api/projects.html)
* [Branches](https://docs.gitlab.com/ce/api/branches.html)
* [Tags](https://docs.gitlab.com/ce/api/tags.html)
* [Repositories](https://docs.gitlab.com/ce/api/repositories.html)
* [Repository Files](https://docs.gitlab.com/ce/api/repository_files.html)
* [Commits](https://docs.gitlab.com/ce/api/commits.html)
* [Merge Requests](https://docs.gitlab.com/ce/api/merge_requests.html)
* [Labels](https://docs.gitlab.com/ce/api/labels.html)
* [Services](https://docs.gitlab.com/ce/api/services.html) (partial)

## Usage
```typescript
import { ProjectsApi, Config, Project, loadConfig } from 'gitlab-fetch';

async function run(): Project[] {
  const config: Config = {
    baseUrl: 'https://gitlab.com/api',
    auth: {
      privateToken: process.env.GITLAB_API_TOKEN,
    }
  };
  // Load from `~/.gitlab-fetchrc` (or similar, see https://www.npmjs.com/package/rc)
  // const config = loadConfig();
  const projectsApi = new ProjectsApi(config);
  return await projectsApi.getAll());
}
```
